import unittest
from package_test import Morpion 

"""Classe de test ( une classe de test par methode)"""
class Test_position(unittest.TestCase) :
    def setUp (self): # faire attention attention a mettre un U majuscule 
        self.jeu=Morpion.Morpion() # en premier le nom du modul et le deuxieme est la classe

    def test_ChgtTourok(self):
        grille=[None for i in range(9)]
        grille[0]=self.jeu.joueur[0]
        self.assertEqual(self.jeu.getTour(), 1)
        self.jeu.position(1,1)
        self.assertEqual(self.jeu.getTour(), 2)
        self.assertEqual(self.jeu.getGrille(),grille)

    def test_ChgtTournot(self):
        self.jeu.position(2,2)
        self.assertEqual(self.jeu.getTour(), 1)


if __name__ == "__main__":
    unittest.main()
    
    



